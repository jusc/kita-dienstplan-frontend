import React from "react";
import { IonReactRouter } from "@ionic/react-router";
import { addDecorator } from "@storybook/react";
import { action } from "@storybook/addon-actions";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "../src/theme/variables.css";
// custom modules
import '../src/index.css'

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};

// add redux support
import { Provider } from "react-redux";
import { initialState as authState } from '../src/data/slices/authSlice'
// import store from "../src/data/store";

// A super-simple mock of a redux store
const store = {
  getState: () => {
    return {auth: authState};
  },
  subscribe: () => 0,
  dispatch: action("dispatch"),
};

// wrap all the stories in the Ionic App Page Structure
const ReduxWrapper = ({ children }) => {
  return (
    <Provider store={store}>
      <IonReactRouter>
        <div>{children}</div>
      </IonReactRouter>
    </Provider>
  );
};

addDecorator((storyFn) => <ReduxWrapper>{storyFn()}</ReduxWrapper>);
