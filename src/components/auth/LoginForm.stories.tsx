import React from "react";
import { action } from "@storybook/addon-actions";

import { LoginForm } from "./LoginForm";
import { Error } from '../../models/Error'

export default {
  component: LoginForm,
  title: "LoginForm",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const storyData = {
  loading: false,
  error: null
};

export const actionsData = {
  onLogin: action("onLogin"),
};

const errorData: Error = {
  type: "test/login",
  message: "Invalid password.",
  name: "errorName",
  code: "errorCode",
}

export const Default = () => <LoginForm {...storyData} {...actionsData} />;

export const Loading = () => (
  <LoginForm {...storyData} loading={true} {...actionsData} />
);

export const InvalidLogin = () => (
  <LoginForm {...storyData} {...actionsData} error={errorData}/>
);
