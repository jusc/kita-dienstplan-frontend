import React from "react";
import { action } from "@storybook/addon-actions";

import { SignupForm } from "./SignupForm";
import { Error } from '../../models/Error'

export default {
  component: SignupForm,
  title: "SignupForm",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const storyData = {
  loading: false,
  error: null
};

export const actionsData = {
  onSignup: action("onSignup"),
};

const errorData: Error = {
  type: "test/signup",
  message: "Invalid password.",
  name: "errorName",
  code: "errorCode",
}

export const Default = () => <SignupForm {...storyData} {...actionsData} />;

export const Loading = () => (
  <SignupForm {...storyData} loading={true} {...actionsData} />
);

export const InvalidSignup = () => (
  <SignupForm {...storyData} {...actionsData} error={errorData}/>
);
