import React from "react";
import { connect } from "react-redux";
import {
  IonCard,
  IonCardHeader,
  IonList,
  IonInput,
  IonText,
  IonCardTitle,
  IonCardContent,
  IonItem,
  IonLabel,
  IonButton,
  IonLoading,
  IonRouterLink,
} from "@ionic/react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers";
import * as yup from "yup";
// own imports
import "./AuthForm.css";
import { RootState } from "../../data/rootReducer";
import { login, selectLoading, selectError } from "../../data/slices/authSlice";
import { Error } from "../../models/Error";
import useScreensize from "../../utils/useScreensize";

// Component properties
interface StateProps {
  loading: boolean;
  error: Error | null;
}
interface DispatchProps {
  onLogin: (email: string, password: string) => any;
}
interface OwnProps {}

type LoginFormProps = StateProps & DispatchProps & OwnProps;

// Login input data schema
type LoginInputs = {
  email: string;
  password: string;
};

const loginSchema = yup.object().shape({
  email: yup
    .string()
    .email("No valid email")
    .required("This is a required field"),
  password: yup.string().required("This is a required field"),
});

// Login form component
export const LoginForm: React.FC<LoginFormProps> = ({
  loading,
  error,
  onLogin,
}) => {
  // get screen width
  const { width } = useScreensize();

  // Form and validation initialization
  const { register, handleSubmit, errors } = useForm<LoginInputs>({
    resolver: yupResolver(loginSchema),
  });

  const onSubmit = async (data: LoginInputs) => {
    // Call login function
    if (!loading) {
      await onLogin(data.email, data.password);
    }
  };

  return (
    <IonCard className={"auth-form" + (width < 576 ? " sm" : "")}>
      <IonCardHeader>
        <IonCardTitle>Login</IonCardTitle>
      </IonCardHeader>
      <IonCardContent>
        {/* "handleSubmit" will validate your inputs before invoking "onSubmit" */}
        <form onSubmit={handleSubmit(onSubmit)}>
          <IonList>
            <IonItem>
              <IonLabel position="stacked" color="primary">
                E-Mail
              </IonLabel>
              <IonInput
                name="email"
                type="text"
                inputmode="email"
                ref={register}
              ></IonInput>
            </IonItem>
            {errors.email && (
              <IonText color="danger">
                <p className="ion-padding-start">{errors.email?.message}</p>
              </IonText>
            )}

            <IonItem>
              <IonLabel position="stacked" color="primary">
                Password
              </IonLabel>
              <IonInput
                name="password"
                type="password"
                ref={register}
              ></IonInput>
            </IonItem>
            {errors.password && (
              <IonText color="danger">
                <p className="ion-padding-start">{errors.password?.message}</p>
              </IonText>
            )}
            {error != null && error.type.includes("login") && (
              <IonText color="danger">
                <p className="ion-padding-start">{error.message}</p>
              </IonText>
            )}

            <div className="button-wrapper">
              <IonButton type="submit">Login</IonButton>
            </div>
          </IonList>
          <div className="auth-link">
            <IonRouterLink routerLink="/signup">
              Not yet registered?
            </IonRouterLink>
          </div>
        </form>
        <IonLoading isOpen={loading} message={"Please wait..."} />
      </IonCardContent>
    </IonCard>
  );
};

// Connect component properties to state properties
function mapStateToProps(state: RootState, ownProps: OwnProps) {
  const loading = selectLoading(state);
  const error = selectError(state);
  return { loading: loading, error: error };
}

// Connect actions to component functions
const mapDispatch = {
  onLogin: (email: string, password: string) =>
    login({ email: email, password: password }),
};
// #const connector = connect(mapStateToProps, mapDispatch);
const connector = connect(mapStateToProps, mapDispatch);

export default connector(LoginForm);
