import React, {useEffect} from "react";
import { IonApp } from "@ionic/react";
import { connect, useDispatch, useSelector } from "react-redux";
import { RootState } from "./data/rootReducer";
import { IonLoading } from "@ionic/react"
// Own imports
import AuthContent from "./AuthContent";
import NoAuthContent from "./NoAuthContent";
import { getSession, selectLoading } from "./data/slices/authSlice"

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";

interface StateProps {
  user: any;
}
interface DispatchProps {}
interface OwnProps {}

type AppProps = StateProps & DispatchProps & OwnProps;

const App: React.FC<AppProps> = ({ user }) => {

  // loading state of session initialization
  const loading = useSelector(selectLoading)
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getSession())
  }, [dispatch]);
    

  return (
    loading ? (
      <IonLoading isOpen={loading}/>)
    : 
    <IonApp>
      {(user != null) ? (<AuthContent />) : (<NoAuthContent />)}
    </IonApp>
  );
};

// Connect component properties to state properties
function mapStateToProps(state: RootState, ownProps: OwnProps) {
  const user = state.auth.user;
  return { user: user };
}

// Connect actions to component functions
const mapDispatch = {};

const connector = connect(mapStateToProps, mapDispatch);

export default connector(App);
