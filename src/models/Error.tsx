export interface Error {
    type: string,
    message: string,
    name: string,
    code: string
}


export const createActionError = (action: any) => {
    return {
        type: action.type,
        message: action.error.message,
        name: action.error.name,
        code: action.error.code
    }
}