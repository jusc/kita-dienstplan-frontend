export interface Request {
    status: RequestStatus
}

// Status API request
export enum RequestStatus {
    Idle = "idle",
    Loading = "loading",
    Succeeded = "succeeded",
    Failed = "failed"
}