import { IonContent, IonPage } from "@ionic/react";
import React from "react";

// own imports
import "./Auth.css";
import LoginForm from "../components/auth/LoginForm";
import SignupForm from "../components/auth/SignupForm";
import useScreensize from "../utils/useScreensize"


// Component properties
interface StateProps {}
interface DispatchProps {}
interface OwnProps {
  formType: string;
}

type AuthProps = StateProps & DispatchProps & OwnProps;


export const Auth: React.FC<AuthProps> = ({formType}) => {

  const {width} = useScreensize()

  // identify form which has to be displayed
  const form = (type: string) => {
  switch (formType) {
    case 'login':
      return <LoginForm />
    case 'signup':
      return <SignupForm />
    default:
      return <LoginForm />
  }
  }


  return (
    <IonPage>
      <IonContent fullscreen>
        <div className={(width<576) ? "auth-page-sm" : "auth-page"}>
          {form(formType)}
        </div>
      </IonContent>
    </IonPage>
  );
};

export default Auth;
