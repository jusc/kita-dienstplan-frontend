import React from "react";

import { Auth } from "./Auth";

export default {
  component: Auth,
  title: "Auth",
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const storyData = {
  formType: 'login'
};

export const actionsData = {};

export const Default = () => <Auth {...storyData} {...actionsData} />;

export const Signup = () => (
<Auth {...storyData} formType='signup' {...actionsData} />
);

// const errorMessage = "Email or password incorrect!"

// export const InvalidLogin = () => (
//   <LoginForm {...storyData} request={{...loginData.request, status: RequestStatus.Failed, error: errorMessage}} {...actionsData} />
// );
