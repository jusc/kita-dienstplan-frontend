// Template from https://blog.logrocket.com/developing-responsive-layouts-with-react-hooks/


// Ionic breakpoints width
// xs 	
// sm 	576px
// md 	768px
// lg 	992px
// xl 	1200px

import { useState, useEffect } from 'react';

const useScreensize = () => {
  const [width, setWidth] = useState(window.innerWidth);
  const [height, setHeight] = useState(window.innerHeight);
  const handleWindowResize = () => {
    setWidth(window.innerWidth);
    setHeight(window.innerHeight);
  };

  useEffect(() => {
    window.addEventListener("resize", handleWindowResize);
    return () => window.removeEventListener("resize", handleWindowResize);
  }, []);

  return ({width: width, height: height})
};


export default useScreensize