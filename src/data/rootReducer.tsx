// Create the root reducer function.
// Need to know what the TypeScript type is for that root state object, because we need to declare what the type of the state variable is whenever our code needs to access the Redux store state (such as in mapState functions, useSelector selectors, and getState in thunks).
import { combineReducers } from "@reduxjs/toolkit";
import authReducer from './slices/authSlice'

const rootReducer = combineReducers({
    auth: authReducer,
});


export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
