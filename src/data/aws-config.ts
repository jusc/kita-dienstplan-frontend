export default {
    s3: {
      REGION: "eu-central-1",
      BUCKET: "kita-dienstplan-api-prod-serverlessdeploymentbuck-1oop9tdhn5ybl"
    },
    apiGateway: {
      REGION: "eu-central-1",
      URL: "https://4x7m896yy4.execute-api.eu-central-1.amazonaws.com/prod"
    },
    cognito: {
      REGION: "eu-central-1",
      USER_POOL_ID: "eu-central-1_QZg3OciNg",
      APP_CLIENT_ID: "3n9ts1ilcpcb44sosf5l0rrchf",
      IDENTITY_POOL_ID: "eu-central-1:4ca5e7d0-7901-4f61-a405-50ee7488a39b"
    }
  };
