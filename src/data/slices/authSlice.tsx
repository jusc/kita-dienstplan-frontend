import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { RootState } from "../rootReducer";
import { Auth } from "aws-amplify";
// models and functions
import { Request, RequestStatus } from "../../models/Request";
import { User } from "../../models/User";
import { Error, createActionError } from "../../models/Error";

// Auth state properties
interface State {
  user: User | null;
  request: Request;
  error: Error | null;
}

// Initial state
export const initialState: State = {
  user: { email: "test" },
  request: { status: RequestStatus.Idle },
  error: null,
};

// API calls

// LOGIN
// Payload for login
interface LoginPayload {
  email: string;
  password: string;
}

// Login api call
export const login = createAsyncThunk(
  "auth/login",
  async (loginData: LoginPayload) => {
    const { email, password } = loginData;
    const response = await Auth.signIn(email, password);
    const user: User = { email: response.attributes.email };
    return user;
  }
);

// SIGNUP
// Payload for Signup
interface SignupPayload {
  email: string;
  password: string;
}

// Signup api call
export const signup = createAsyncThunk(
  "auth/signup",
  async (signupData: SignupPayload) => {
    const { email, password } = signupData;
    console.log("Signup " + email + password);
    // const response = await Auth.signIn(email, password);
    // return response;
  }
);

// LOGOUT
// Logout api call
export const logout = createAsyncThunk("auth/logout", async () => {
  const response = await Auth.signOut();
  return response;
});

// SESSION
// Get session api call
export const getSession = createAsyncThunk("auth/session", async () => {
  const response = await Auth.currentSession();
  const userProps = response.getIdToken().payload;
  const user: User = { email: userProps.email };
  return user;
});

// Slice
const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    clearRequest: (state) => {
      state.request = { status: RequestStatus.Idle };
    },
    clearError: (state) => {
      state.error = null;
    },
  },
  // actions that are dispatched depending on http request
  extraReducers: (builder) => {
    // LOGIN REQUEST
    // request started
    builder.addCase(login.pending, (state, action) => {
      state.request.status = RequestStatus.Loading;
      state.error = null;
    });
    // login successfull
    builder.addCase(login.fulfilled, (state, action) => {
      state.request.status = RequestStatus.Succeeded;
      state.user = action.payload;
    });
    // login failed
    builder.addCase(login.rejected, (state, action) => {
      state.request.status = RequestStatus.Failed;
      state.error = createActionError(action);
    });
    // SIGNUP REQUEST
    // signup started
    builder.addCase(signup.pending, (state, action) => {
      state.request.status = RequestStatus.Loading;
      state.error = null;
    });
    // signup successfull
    builder.addCase(signup.fulfilled, (state, action) => {
      state.request.status = RequestStatus.Succeeded;
      console.log("Signup Successful");
    });
    // signup failed
    builder.addCase(signup.rejected, (state, action) => {
      state.request.status = RequestStatus.Failed;
      state.error = createActionError(action);
    });
    // LOGOUT REQUEST
    // logout successfull
    builder.addCase(logout.fulfilled, (state, action) => {
      state.request.status = RequestStatus.Succeeded;
      state.user = null;
    });
    // logout failed
    builder.addCase(logout.rejected, (state, action) => {
      state.request.status = RequestStatus.Failed;
      state.error = createActionError(action);
    });
    // SESSION REQUEST
    // session load successfull
    builder.addCase(getSession.fulfilled, (state, action) => {
      state.request.status = RequestStatus.Succeeded;
      state.user = action.payload;
    });
    // no session
    builder.addCase(getSession.rejected, (state, action) => {
      state.request.status = RequestStatus.Failed;
      state.error = createActionError(action);
      state.user = null;
    });
  },
});

// selector functions
// select user
export const selectUser = (state: RootState) => state.auth.user;
export const selectLoading = (state: RootState) =>
  state.auth.request.status === RequestStatus.Loading;
export const selectError = (state: RootState) => state.auth.error;

// export actions
export const { clearRequest, clearError } = authSlice.actions;

// export reducer
export default authSlice.reducer;
