import React from "react";
import { Redirect, Route } from "react-router-dom";
import { IonRouterOutlet } from "@ionic/react";
import { IonReactRouter } from "@ionic/react-router";
import Auth from "./pages/Auth";

interface StateProps {}
interface DispatchProps {}
interface OwnProps {}

type NoAuthContentProps = StateProps & DispatchProps & OwnProps;

const NoAuthContent: React.FC<NoAuthContentProps> = () => (
  <IonReactRouter>
    <IonRouterOutlet>
      <Route path="/login" exact={true}>
        <Auth formType="login" />
      </Route>
      <Route path="/signup" exact={true}>
        <Auth formType="signup" />
      </Route>
      <Route exact path="/" render={() => <Redirect to="/login" />} />
      <Redirect to="/" />
    </IonRouterOutlet>
  </IonReactRouter>
);

export default NoAuthContent;
