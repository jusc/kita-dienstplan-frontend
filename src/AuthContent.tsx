import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';


interface StateProps {}
interface DispatchProps {}
interface OwnProps {}

type AuthContentProps = StateProps & DispatchProps & OwnProps;


const AuthContent: React.FC<AuthContentProps> = () => (
    <IonReactRouter>
        <IonRouterOutlet>
            
          <Route path="/home" component={Home} exact={true} />
          <Route exact path="/" render={() => <Redirect to="/home" />} />
          <Redirect to="/" />
        </IonRouterOutlet>
        </IonReactRouter>
  );
  
  export default AuthContent; 
  