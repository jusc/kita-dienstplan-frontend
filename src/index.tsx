// for setup see
// https://redux-toolkit.js.org/tutorials/advanced-tutorial
// https://redux.js.org/tutorials/essentials/part-3-data-flow

import React from "react";
import ReactDOM from "react-dom";
import store from "./data/store";
import { Provider } from "react-redux";
import * as serviceWorker from "./serviceWorker";

// custom modules
import './index.css'

// AWS
import { Amplify } from 'aws-amplify';
import config from './data/aws-config';


// https://serverless-stack.com/chapters/configure-aws-amplify.html
Amplify.configure({
  Auth: {
    mandatorySignIn: true,
    region: config.cognito.REGION,
    userPoolId: config.cognito.USER_POOL_ID,
    identityPoolId: config.cognito.IDENTITY_POOL_ID,
    userPoolWebClientId: config.cognito.APP_CLIENT_ID
  },
  Storage: {
    region: config.s3.REGION,
    bucket: config.s3.BUCKET,
    identityPoolId: config.cognito.IDENTITY_POOL_ID
  },
  API: {
    endpoints: [
      {
        name: "employees",
        endpoint: config.apiGateway.URL,
        region: config.apiGateway.REGION
      },
    ]
  }
});



const render = () => {
  const App = require("./App").default;

  ReactDOM.render(
      <Provider store={store}>
        <App />
      </Provider>
      ,
    document.getElementById("root")
  );
};

render();

// development mode
if (process.env.NODE_ENV === "development" && module.hot) {
  // activate hot reload
  module.hot.accept("./App", render);
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
